import React, {Component} from 'react';
import axios from 'axios';
import {Link, Redirect} from 'react-router-dom';
import TitleComponent from "./title";
// import logo from "./pages/iteria_token.png"
// import "./App.css";

export default class Login extends Component {

    state = {
        email: '',
        password: '',
        redirect: false,
        authError: false,
        isLoading: false,
        location: {},
    };

    handleEmailChange = event => {
        this.setState({email: event.target.value});
    };
    handlePwdChange = event => {
        this.setState({password: event.target.value});
    };

    handleSubmit = event => {
        event.preventDefault();
        this.setState({isLoading: true});
        const url = 'https://gowtham-rest-api-crud.herokuapp.com/login';
        const email = this.state.email;
        const password = this.state.password;
        let bodyFormData = new FormData();
        bodyFormData.set('email', email);
        bodyFormData.set('password', password);
        axios.post(url, bodyFormData)
            .then(result => {
                if (result.data.status) {
                    localStorage.setItem('token', result.data.token);
                    this.setState({redirect: true, isLoading: false});
                    localStorage.setItem('isLoggedIn', true);
                }
            })
            .catch(error => {
                console.log(error);
                this.setState({authError: true, isLoading: false});
            });
    };

    componentDidMount() {
        const url = 'https://freegeoip.app/json/';
        axios.get(url)
            .then(response => {
                const location = response.data;
                this.setState({ location });
            }) 
            .catch(error => {
                this.setState({ toDashboard: true });
                console.log(error);
            });
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/dashboard'/>
        }
    };

    render() {
        const isLoading = this.state.isLoading;
        return (
           
            <div className="container">
                <TitleComponent title="XRP_wallet"></TitleComponent>

                <div class="row">
                    <div class="col">
                        {/* column 1 data: Logo login */}
                        <div className="card card-login mx-auto mt-5">
                            <img src="iteria_token.png" alt="iteria_token_logo" width="400" height="200" align="center"></img>
                            {/* <img src={require('iteria_token.png')}></img> */}
                            {/* <img src={logo} alt="Logo" /> */}
                        </div>
                    </div>

                    <div class="col">
                        {/* Column 2 data: Login form */}
                        <div className="card card-login mx-auto mt-5">
                        <div className="text-center">
                            <h3>Login to GCIT Wallet</h3>
                        </div>
                    
                        <div className="card-body">
                            {/* <div className='container'>
                                <span> Hello </span>
                            </div> */}
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <div className="form-label-group">
                                        <input className={"form-control " + (this.state.authError ? 'is-invalid' : '')} id="inputEmail" placeholder="CID" type="text" name="email" onChange={this.handleEmailChange} autoFocus required/>
                                        <label htmlFor="inputEmail">CID</label>
                                        <div className="invalid-feedback">
                                            Please provide a valid Email.
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-label-group">
                                        <input type="password" className={"form-control " + (this.state.authError ? 'is-invalid' : '')} id="inputPassword" placeholder="******" name="password" onChange={this.handlePwdChange} required/>
                                        <label htmlFor="inputPassword">Password</label>
                                        <div className="invalid-feedback">
                                            Please provide a valid Password.
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="checkbox">
                                        {/* <label className='forgotPassword'>
                                        Forgot your password?
                                        </label> */}
                                        <a className="d-block small" href="forgot-password.html">Forgot Password?</a>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <button className="btn btn-primary mb1 bg-black btn-block " type="submit" color="#573131" disabled={this.state.isLoading ? true : false}>Login &nbsp;&nbsp;&nbsp;
                                        {isLoading ? (
                                            <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        ) : (
                                            <span></span>
                                        )}
                                    </button>
                                </div>
                                {/* <div className="form-group">
                                    <div className="form-group">
                                        <b>email:</b> gowthaman.nkl1@gmail.com
                                    </div>
                                    <div className="form-group">
                                        <b>password :</b> password
                                    </div>
                                </div> */}
                            </form>
                            <div className="text-center">
                                <span>Do you have an account?</span><Link className="d-block small mt-3" to={'register'}>Register</Link>
                                
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
               
        
                {this.renderRedirect()}
            </div>
        );
    }
}


